
package polimorfismo;

public class MAIN {
    public static void main(String[] args) {
        veiculo misVeiculos[]=new veiculo[3];
        
       misVeiculos[0]= new veiculo("45gg","ferrari","2021");
       misVeiculos[1]= new veiculoTurismo(4,"gh56","chevrolet","2004");
       misVeiculos[2]= new  veiculoNietaFurgoneta(6,"hh67","volkswagen","1995","10 pasajeros");
      
       
       for(veiculo v:misVeiculos){
        System.out.println(v.mostraDatos());
    }
    
}
}