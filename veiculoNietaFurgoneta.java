
package polimorfismo;


public class veiculoNietaFurgoneta extends veiculoTurismo {
    private String capasidadPasajeros;
    
    public veiculoNietaFurgoneta(int numeroPuertas, String matricula, String marca, String modelo,String capasidadPasajeros) {
    super(numeroPuertas, matricula, marca, modelo);
    this.capasidadPasajeros=capasidadPasajeros;
    
    
    
    }

    public String getCapasidadPasajeros() {
        return capasidadPasajeros;
    }
    
    @Override
    public String mostraDatos(){
         return "\nmatricula:"+matricula+"\nmarca:"+marca+"\nmodelo:"+modelo+
               "\ncapasidad de pasajeros:"+capasidadPasajeros;
    
}
}